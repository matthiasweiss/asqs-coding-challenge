import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Department } from '../../models/department';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public users$: BehaviorSubject<User[]>;

  private users: User[] = [];
  private idCounter = 1;

  public constructor() {
    this.users$ = new BehaviorSubject(this.users);

    // uncomment to add example data
    // this.add('johndoe', 'John', 'Doe', true, Department.maintenance);
    // this.add('janedoe', 'Jane', 'Doe', false, Department.marketing);
  }

  public findById(id: number): User | undefined {
    return this.users.find((user) => user.id === id);
  }

  // could have also used Omit<User, 'id'> here, did not know at the time of writing
  public add(username: string, firstName: string, lastName: string, isAdmin: boolean, department: Department): User {
    const newUser: User = {
      id: this.idCounter++,
      username,
      firstName,
      lastName,
      isAdmin,
      department,
    };

    this.users.push(newUser);

    this.users$.next(this.users);

    return newUser;
  }
}
