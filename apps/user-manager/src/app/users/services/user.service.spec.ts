import { TestBed } from '@angular/core/testing';
import { Department } from '../../models/department';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  const username = 'johndoe';
  const firstName = 'john';
  const lastName = 'doe';
  const isAdmin = true;
  const department = Department.maintenance;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should contain an empty list of users after creation', (done) => {
    service.users$.subscribe((users) => {
      expect(users.length).toEqual(0);
      done();
    });
  });

  it('contains a new user after calling #add()', (done) => {
    service.add(username, firstName, lastName, isAdmin, department);

    service.users$.subscribe((users) => {
      expect(users.length).toEqual(1);
      expect(users[0].username).toEqual(username);
      expect(users[0].firstName).toEqual(firstName);
      expect(users[0].lastName).toEqual(lastName);
      expect(users[0].isAdmin).toEqual(isAdmin);
      expect(users[0].department).toEqual(department);
      done();
    });
  });

  it('#add() automatically assigns the ID to a new user', () => {
    const newUser = service.add(username, firstName, lastName, isAdmin, department);

    expect(newUser.id).toEqual(1);
  });

  it('#add() automatically increments the ID for each new user', () => {
    const newUser = service.add(username, firstName, lastName, isAdmin, department);
    let expectedId = 1;
    expect(newUser.id).toEqual(expectedId);

    const otherUser = service.add('janedoe', 'jane', lastName, isAdmin, department);

    expectedId = expectedId + 1;
    expect(otherUser.id).toEqual(expectedId);
  });

  it('#findById() returns the user with the given ID', () => {
    const john = service.add(username, firstName, lastName, isAdmin, department);
    const jane = service.add('janedoe', 'jane', lastName, isAdmin, department);

    expect(service.findById(john.id)?.username).toEqual(john.username);
    expect(service.findById(jane.id)?.username).toEqual(jane.username);
  });

  it('#findById() with non-existing ID returns undefined', () => {
    const nonExistingId = 12345;
    expect(service.findById(nonExistingId)?.username).toEqual(undefined);
  });
});
