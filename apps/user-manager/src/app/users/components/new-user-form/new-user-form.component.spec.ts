import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';

import { NewUserFormComponent } from './new-user-form.component';
import { Department } from '../../../models/department';
import { UserService } from '../../services/user.service';
import { User } from '../../../models/user';

// https://github.com/angular/components/blob/master/guides/using-component-harnesses.md
let loader: HarnessLoader;

const user: User = {
  id: 1,
  username: 'johndoe',
  firstName: 'john',
  lastName: 'doe',
  isAdmin: true,
  department: Department.maintenance,
};

const setInputHarness = async (placeholder: string, value: string) => {
  await (await loader.getHarness(MatInputHarness.with({ placeholder }))).setValue(value);
};

const clickButtonHarness = async (text: string) => {
  await (await loader.getHarness(MatButtonHarness.with({ text }))).click();
};

const selectDepartmentHarness = async () => {
  await (await loader.getHarness(MatSelectHarness)).clickOptions({ text: user.department });
};

const toggleIsAdminHarness = async () => {
  await (await loader.getHarness(MatCheckboxHarness)).toggle();
};

describe('NewUserFormComponent', () => {
  let component: NewUserFormComponent;
  let fixture: ComponentFixture<NewUserFormComponent>;
  let spy: jest.SpyInstance;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewUserFormComponent],
      imports: [RouterTestingModule, SharedModule],
    }).compileComponents();

    fixture = TestBed.createComponent(NewUserFormComponent);
    component = fixture.componentInstance;

    loader = TestbedHarnessEnvironment.loader(fixture);

    // create spy
    spy = jest.spyOn(component.userAdded, 'emit');

    // set valid inputs (department is not set since it cannot be unset)
    await setInputHarness('Username', user.username);
    await setInputHarness('First name', user.firstName);
    await setInputHarness('Last name', user.lastName);
    await toggleIsAdminHarness();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('submits form if inputs are valid', async () => {
    const userServiceSpy = jest.spyOn(TestBed.get(UserService), 'add');

    // submit the form by selecting department and clicking the button
    await selectDepartmentHarness();
    await clickButtonHarness('Add');

    expect(spy).toHaveBeenCalled();
    expect(userServiceSpy).toHaveBeenCalledWith(
      user.username,
      user.firstName,
      user.lastName,
      user.isAdmin,
      user.department
    );
  });

  it('does not submit form if username is empty', async () => {
    await selectDepartmentHarness();
    await setInputHarness('Username', '');

    expect(component.newUserForm.status).toEqual('INVALID');
    expect(component?.newUserForm?.controls?.username?.errors?.required).toBeTruthy();
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('does not submit form if first name is empty', async () => {
    await selectDepartmentHarness();
    await setInputHarness('First name', '');

    expect(component.newUserForm?.status).toEqual('INVALID');
    expect(component?.newUserForm?.controls?.firstName?.errors?.required).toBeTruthy();
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('does not submit form if last name is empty', async () => {
    await selectDepartmentHarness();
    await setInputHarness('Last name', '');

    expect(component.newUserForm?.status).toEqual('INVALID');
    expect(component?.newUserForm?.controls?.lastName?.errors?.required).toBeTruthy();
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('does not submit form if department is empty', () => {
    expect(component.newUserForm?.status).toEqual('INVALID');
    expect(component?.newUserForm?.controls?.department?.errors?.required).toBeTruthy();
    expect(spy).toHaveBeenCalledTimes(0);
  });
});
