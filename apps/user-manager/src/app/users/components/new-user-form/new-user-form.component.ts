import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Department } from '../../../models/department';
import { User } from '../../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-asqs-assignment-new-user-form',
  templateUrl: './new-user-form.component.html',
  styleUrls: ['./new-user-form.component.scss'],
})
export class NewUserFormComponent {
  @Output()
  public cancel = new EventEmitter();

  @Output()
  public userAdded = new EventEmitter<User>();

  public departments = Department;
  public newUserForm: FormGroup;

  // status options are defined in @angular/forms/forms.d.ts
  private readonly VALID_FORM_STATUS = 'VALID';

  public constructor(private fb: FormBuilder, private userService: UserService) {
    this.fb = fb;
    this.userService = userService;

    this.newUserForm = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      department: ['', Validators.required],
      isAdmin: [false],
    });
  }

  public emitCancelEvent(): void {
    this.cancel.emit();
  }

  public onSubmit(): void {
    if (this.newUserForm.status === this.VALID_FORM_STATUS) {
      const newUser: User = this.userService.add(
        this.newUserForm.controls.username.value,
        this.newUserForm.controls.firstName.value,
        this.newUserForm.controls.lastName.value,
        this.newUserForm.controls.isAdmin.value,
        this.newUserForm.controls.department.value
      );

      this.userAdded.emit(newUser);
    }
  }
}
