import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../../shared/shared.module';

import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatButtonHarness } from '@angular/material/button/testing';

import { UserListComponent } from './user-list.component';
import { UsersModule } from '../../users.module';
import { Department } from '../../../models/department';
import { User } from '../../../models/user';
import { UserService } from '../../services/user.service';
import { BehaviorSubject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

// https://github.com/angular/components/blob/master/guides/using-component-harnesses.md
let loader: HarnessLoader;

let userService: UserService;

// store selector of NewUserFormComponent
const NEW_USER_FORM_SELECTOR = 'app-asqs-assignment-new-user-form';

const john: User = {
  id: 1,
  username: 'johndoe',
  firstName: 'john',
  lastName: 'doe',
  isAdmin: true,
  department: Department.maintenance,
};

const jane: User = {
  id: 2,
  username: 'janedoe',
  firstName: 'jane',
  lastName: 'doe',
  isAdmin: true,
  department: Department.maintenance,
};

// copied from NewUserFormComponent test, shared helper methods?
const clickButtonHarness = async (text: string) => {
  await (await loader.getHarness(MatButtonHarness.with({ text }))).click();
};

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserListComponent],
      imports: [RouterTestingModule, SharedModule, UsersModule],
      providers: [
        {
          provide: UserService,
          useValue: {
            users$: jest.fn(),
          },
        },
      ],
    }).compileComponents();

    userService = TestBed.inject(UserService);

    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;

    loader = TestbedHarnessEnvironment.loader(fixture);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('shows the header', () => {
    expect((fixture.nativeElement as HTMLElement).querySelector('h1')?.textContent).toEqual('Welcome to user-manager!');
  });

  it('shows message if there no users in the system', () => {
    userService.users$ = new BehaviorSubject<User[]>([]);
    fixture.detectChanges();

    expect((fixture.nativeElement as HTMLElement).querySelector('#no-users-message')?.textContent).toContain(
      'There are currently no users in the system!'
    );
  });

  it('shows a list of all existing users', () => {
    const users: User[] = [john, jane];
    userService.users$ = new BehaviorSubject<User[]>(users);
    fixture.detectChanges();

    const listItems = (fixture.nativeElement as HTMLElement).querySelectorAll('a.mat-list-item');
    expect(listItems.length).toEqual(users.length);
    expect(listItems[0].textContent).toContain(john.username);
    expect(listItems[1].textContent).toContain(jane.username);
  });

  it('hides message when "Add User" button is clicked', async () => {
    userService.users$ = new BehaviorSubject<User[]>([]);
    fixture.detectChanges();

    expect((fixture.nativeElement as HTMLElement).querySelector('#no-users-message')?.textContent).toContain(
      'There are currently no users in the system!'
    );

    await clickButtonHarness('Add User');
    fixture.detectChanges();

    expect((fixture.nativeElement as HTMLElement).querySelector('#no-users-message')).toBeFalsy();
  });

  it('shows the form to add new users when the "Add User" button is clicked', async () => {
    userService.users$ = new BehaviorSubject<User[]>([]);

    await clickButtonHarness('Add User');
    fixture.detectChanges();

    expect((fixture.nativeElement as HTMLElement).querySelector(NEW_USER_FORM_SELECTOR)).toBeTruthy();
  });

  it('hides the form to add new users when the "Cancel" button is clicked', async () => {
    userService.users$ = new BehaviorSubject<User[]>([]);

    await clickButtonHarness('Add User');
    fixture.detectChanges();

    expect((fixture.nativeElement as HTMLElement).querySelector(NEW_USER_FORM_SELECTOR)).toBeTruthy();

    await clickButtonHarness('Cancel');
    fixture.detectChanges();

    expect((fixture.nativeElement as HTMLElement).querySelector(NEW_USER_FORM_SELECTOR)).toBeFalsy();
  });

  it('calls #onUserAdded() function when it receives userAdded event', () => {
    userService.users$ = new BehaviorSubject<User[]>([]);
    component.newUserFormShown = true;
    fixture.detectChanges();
    const spy = jest.spyOn(component, 'onUserAdded');

    component.newUserForm.userAdded.emit(john);

    expect(spy).toHaveBeenCalledWith(john);
  });
});
