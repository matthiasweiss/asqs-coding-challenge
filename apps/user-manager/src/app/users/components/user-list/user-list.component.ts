import { Component, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from '../../../models/user';
import { NewUserFormComponent } from '../new-user-form/new-user-form.component';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-asqs-assignment-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent {
  @ViewChild(NewUserFormComponent)
  public newUserForm!: NewUserFormComponent;
  public newUserFormShown = false;

  public constructor(public userService: UserService, private snackBar: MatSnackBar) {}

  public showNewUserForm(): void {
    this.newUserFormShown = true;
  }

  public hideNewUserForm(): void {
    this.newUserFormShown = false;
  }

  public onUserAdded(user: User): void {
    this.newUserFormShown = false;

    this.snackBar.open(`User "${user.username}" was added successfully!`, '', { duration: 3000 });
  }
}
