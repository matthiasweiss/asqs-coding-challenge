import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';
import { Department } from '../../../models/department';
import { User } from '../../../models/user';
import { SharedModule } from '../../../shared/shared.module';
import { UserService } from '../../services/user.service';

import { UserDetailsComponent } from './user-details.component';

const id = 1;

const user: User = {
  id,
  username: 'johndoe',
  firstName: 'john',
  lastName: 'doe',
  isAdmin: true,
  department: Department.maintenance,
};

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;
  let userService: UserService;
  let spy: jest.SpyInstance;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: UserService,
          useValue: {
            users$: jest.fn(),
            findById: jest.fn(() => user),
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of({
              id,
              get: () => id,
            }),
          },
        },
      ],
      declarations: [UserDetailsComponent],
      imports: [RouterTestingModule, SharedModule],
    }).compileComponents();

    userService = TestBed.inject(UserService);

    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    spy = jest.spyOn(userService, 'findById');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('shows the properties of the user with the given ID', () => {
    userService.users$ = new BehaviorSubject([user]);
    fixture.detectChanges();
    const element = fixture.debugElement;

    expect((element.query(By.css('#id'))?.nativeElement as HTMLElement).textContent).toEqual(String(user.id));
    expect((element.query(By.css('#username'))?.nativeElement as HTMLElement).textContent).toEqual(
      String(user.username)
    );
    expect((element.query(By.css('#firstName'))?.nativeElement as HTMLElement).textContent).toEqual(
      String(user.firstName)
    );
    expect((element.query(By.css('#lastName'))?.nativeElement as HTMLElement).textContent).toEqual(
      String(user.lastName)
    );
    expect((element.query(By.css('#isAdmin'))?.nativeElement as HTMLElement).textContent).toEqual(String(user.isAdmin));
    expect((element.query(By.css('#department'))?.nativeElement as HTMLElement).textContent).toEqual(
      String(user.department)
    );
    expect(spy).toHaveBeenCalledWith(id);
  });

  it('shows not found page for non-existing user ID', () => {
    userService.users$ = new BehaviorSubject<User[]>([]);
    userService.findById = () => undefined;
    // spy has to be set again since findById is overwritten in line above
    spy = jest.spyOn(userService, 'findById');
    fixture.detectChanges();
    const element = fixture.debugElement;

    expect((element.query(By.css('#notFound'))?.nativeElement as HTMLElement).textContent).toContain(
      `User with ID "${id}" does not exist!`
    );
    expect(spy).toHaveBeenCalledWith(id);
  });
});
