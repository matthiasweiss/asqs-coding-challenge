import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { User } from '../../../models/user';
import { UserService } from '../../services/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-asqs-assignment-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent implements OnInit {
  public user?: User;
  public userId!: number;

  public constructor(private userService: UserService, private route: ActivatedRoute) {}

  public ngOnInit(): void {
    // find user using the ID route parameter
    this.route.paramMap
      .pipe(
        map((params: ParamMap) => {
          this.userId = Number(params.get('id'));
          return this.userService.findById(this.userId);
        })
      )
      .subscribe((user) => {
        this.user = user;
      });
  }
}
