// eslint disabled since no-shadow false positive for enums
// https://github.com/typescript-eslint/tslint-to-eslint-config/issues/856

// eslint-disable-next-line no-shadow
export enum Department {
  marketing = 'Marketing',
  management = 'Management',
  maintenance = 'Maintenance',
}
