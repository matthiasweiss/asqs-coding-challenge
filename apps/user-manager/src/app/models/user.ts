import { Department } from './department';

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  isAdmin: boolean;
  department: Department;
}
