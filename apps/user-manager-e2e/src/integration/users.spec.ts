import { first } from 'cypress/types/lodash';
import { getGreeting } from '../support/app.po';

describe('user-manager', () => {

  const addUser = (username, firstName, lastName, isAdmin) => {
    cy.get('button').click();
    cy.get('input[formcontrolname=username]').click().type(username);
    cy.get('input[formcontrolname=firstName]').click().type('john');
    cy.get('input[formcontrolname=lastName]').click().type('doe');
    cy.get('mat-select[formControlName=department]').click().get('mat-option').contains('Maintenance').click();
    if (isAdmin) {
      cy.get('mat-checkbox').click();
    }
    cy.get('button[type="submit"]').click();
  };

  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    cy.get('h1').contains('Welcome to user-manager!');
  });

  it('should display message that no users currently exist', () => {
    cy.get('body').contains('There are currently no users in the system!');
  });

  it('should display button to add new users', () => {
    cy.get('button').contains('Add User');
  });

  it('should hide message when button is clicked', () => {
    cy.get('body').contains('There are currently no users in the system!');
    cy.get('button').click();
    cy.get('#no-users-message').should('not.exist');
  });

  it('should show form to add new user when button is clicked', () => {
    cy.get('body').contains('There are currently no users in the system!');
    cy.get('button').click();
    cy.get('form').should('exist');
  });

  it('should hide form after cancel button is clicked', () => {
    cy.get('body').contains('There are currently no users in the system!');
    cy.get('button').click();
    cy.get('form').should('exist');
    cy.get('button[type=button]').click();
    cy.get('form').should('not.exist');
  });

  it('should not be able to submit form until all required fields are filled out', () => {
    cy.get('button').click();

    cy.get('button[type=submit]').click();
    cy.get('form.ng-invalid').should('exist');
    cy.get('input[formcontrolname=username].ng-invalid').should('exist');
    cy.get('input[formcontrolname=firstName].ng-invalid').should('exist');
    cy.get('input[formcontrolname=lastName].ng-invalid').should('exist');
    cy.get('[formcontrolname=department]').should('exist');

    cy.get('input[formcontrolname=username]').click().type('johndoe');
    cy.get('button[type="submit"]').click();
    cy.get('input[formcontrolname=username].ng-invalid').should('not.exist');

    cy.get('input[formcontrolname=firstName]').click().type('john');
    cy.get('button[type="submit"]').click();
    cy.get('input[formcontrolname=firstName].ng-invalid').should('not.exist');

    cy.get('input[formcontrolname=lastName]').click().type('doe');
    cy.get('button[type="submit"]').click();
    cy.get('input[formcontrolname=lastName].ng-invalid').should('not.exist');

    cy.get('mat-select[formControlName=department]').click().get('mat-option').contains('Maintenance').click();
    cy.get('button[type="submit"]').click();
    cy.get('[formcontrolname=department]').should('not.exist');

    cy.get('form.ng-invalid').should('not.exist');
  });

  it('should display list containing newly created user if form with valid input is submitted', () => {
    const username = 'johndoe';
    cy.get('body').contains('There are currently no users in the system!');

    addUser(username, 'john', 'doe', false);

    cy.get('#no-users-message').should('not.exist');
    cy.get('a[mat-list-item]').should('exist');
    cy.get('mat-nav-list').contains(username);
  });

  it('should highlight administrators in yellow', () => {
    const username = 'johndoe';
    cy.get('body').contains('There are currently no users in the system!');

    addUser(username, 'john', 'doe', true);

    cy.get('a[mat-list-item].bg-yellow').contains(username);
  });

  it('should display all properties of a user when clicking on the given link', () => {
    const username = 'johndoe';
    const firstName = 'john';
    const lastName = 'doe';
    const isAdmin = true;

    addUser(username, firstName, lastName, isAdmin);

    cy.get('a[mat-list-item]').click();
    cy.get('#username').contains(username);
    cy.get('#firstName').contains(firstName);
    cy.get('#lastName').contains(lastName);
    cy.get('#isAdmin').contains(String(isAdmin));
  });

  it('should display message when there is no user with given ID', () => {
    const nonExistingId = 123456;

    cy.visit(`/details/${nonExistingId}`);

    cy.get('#notFound').contains(nonExistingId);
  });
});
