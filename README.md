# User List

## Project Setup
* Install Node (v14.X) and NPM (v7.x)
* Run `npm i`

## Task

Develop & test a user management application using Angular & Angular Material.

The application consists of three parts:

* A table displaying a simplified list of all existing users
* A detailed view displaying all information for an existing user
* A view to create a new user

A user profile includes the following fields:

* id (number)
* first name (string)
* last name (string)
* username (string)
* if the user is an admin (boolean)
* which department the user belongs to ("Marketing", "Management" or "Maintenance")

## User Stories

**View a List of Users**

As a user, i want to be able to see a table of users that are currently present in the system. The table should contain the user's first name, last name and their department. Furthermore, admins should be highlighted in yellow.

**View User Details**

As a user, i want to see a detailed view of a user by clicking on their name in the list view. The detailed view should display *all* properties a user has. Editing/deleting a user is **not** necessary. The detailed view should be available under the route `/details/:userId`

**Add a New User**

As a user, i want to be able to add an additional user to the system, e.g. via text boxes and an "Add" button. The id should be assigned automatically. All other fields are required.

## Notes

* Try to keep commits organized to help us understand your development approach
* Focus on maintainability, extendability and reusability
* Your application should follow the defined linting rules (`ng lint` should pass when you are done)
* Use `@angular/material` where called for. Other than that, no further styling is necessary
* Test your application using `jest` (`ng test` should pass when you are done)
* *(Optional but recommended)* Use `@ngrx/store` to manage application state. State does not need to be persistent across browser reloads.
* *(Optional)* Use `cypress` to e2e test your application 

